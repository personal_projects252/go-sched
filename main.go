package main

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/xuri/excelize/v2"
)

type weekday int

const (
	monday weekday = iota
	tuesday
	wednesday
	thursday
	friday
	saturday
)

var weekdays = [6]weekday{
	monday,
	tuesday,
	wednesday,
	thursday,
	friday,
	saturday,
}

var weekdaysMap = map[weekday]string{
	monday: "Понедельник",
	tuesday: "Вторник",
	wednesday: "Среда",
	thursday: "Четверг",
	friday: "Пятница",
	saturday: "Суббота",
}

type Type int

const (
	lecture Type = iota
	practice
	laboratory
	self
)

type Subject struct {
	name      string
	stype     string
	lecturer  string
	classroom string
}

type Week struct {
	days  map[weekday][]Subject
	isOdd bool
}

// NewWeek returns week with given parity.
func NewWeek(isOdd bool) *Week {
	return &Week{
		days:  make(map[weekday][]Subject),
		isOdd: isOdd,
	}
}

func main() {
	weeks, err := parseTable("schedules/*.xlsx", "ККСО-01-20")
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v", err)
		return
	}

	_ = weeks

	// for _, weekday := range weekdays {
	// 	fmt.Println("\n", weekdaysMap[weekday], ":")
	// 	for index, subject := range weeks[0].days[weekday] {
	// 		fmt.Println(index+1, subject)
	// 	}
	// 	fmt.Println()
	// 	for index, subject := range weeks[1].days[weekday] {
	// 		fmt.Println(index+1, subject)
	// 	}
	// }
}

func parseTable(path string, group string) ([]*Week, error) {
	f, err := openTableFile(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := f.Close(); err != nil {
			fmt.Fprintf(os.Stderr, "%v", err)
		}
	}()

	cols, err := getGroupSched(f, group)
	if err != nil {
		return nil, err
	}

	// FIXME: sched on thursday is off...

	oddWeek := NewWeek(true)
	oddWeek, err = assignSubjects(oddWeek, cols)

	evenWeek := NewWeek(false)
	evenWeek, err = assignSubjects(evenWeek, cols)

	return []*Week{oddWeek, evenWeek}, nil
}

func openTableFile(path string) (*excelize.File, error) {
	filepaths, err := filepath.Glob(path)
	if err != nil {
		return nil, fmt.Errorf("%v", err)
	}
	if len(filepaths) == 0 {
		return nil, fmt.Errorf("files not found")
	}

	f, err := excelize.OpenFile(filepaths[0])
	if err != nil {
		return nil, fmt.Errorf("%v", err)
	}

	return f, nil
}

func getGroupSched(file *excelize.File, group string) ([][]string, error) {
	sheets := file.GetSheetList()

	var colIndex int = -1
	var sheetName string
sheetFor:
	for _, sheetName = range sheets {
		rows, err := file.Rows(sheetName)
		if err != nil {
			return nil, err
		}

		// Group names are on the 2nd row.
		for i := 0; i < 2; i++ {
			rows.Next()
		}

		cols, err := rows.Columns()
		if err != nil {
			return nil, err
		}
		for i, cellValue := range cols {
			if cellValue == group {
				colIndex = i
				break sheetFor
			}
		}
	}

	if colIndex == -1 {
		return nil, fmt.Errorf("groups was not found")
	}

	cols, err := file.Cols(sheetName)
	if err != nil {
		return nil, err
	}
	for i := 0; i < colIndex; i++ {
		cols.Next()
	}
	result := make([][]string, 0, 4)
	for i := 0; i < 4; i++ {
		cols.Next()
		rows, err := cols.Rows()
		if err != nil {
			return nil, err
		}
		result = append(result, rows[3:14*6+3])
	}

	return result, nil
}

// assignSubjects assigns subject from columns to given week.
// Pass initialized with NewWeek() week.
// Pass whole column of subjects.
func assignSubjects(week *Week, cols [][]string) (*Week, error) {
	if week.days == nil {
		return week, fmt.Errorf("uninitialized week variable")
	}

	// There are 14 subjects per day (7 for odd and 7 for even weeks).
	dayOffset := 14
	// Subjects for even week are located on even rows.
	var parityOffset int
	if !week.isOdd {
		parityOffset = 1
	}

	for _, weekday := range weekdays {
		for j := 0; j < 7; j++ {
			week.days[weekday] = append(week.days[weekday], Subject{
				name:      cols[0][dayOffset*int(weekday)+2*j+parityOffset],
				stype:     cols[1][dayOffset*int(weekday)+2*j+parityOffset],
				lecturer:  cols[2][dayOffset*int(weekday)+2*j+parityOffset],
				classroom: cols[3][dayOffset*int(weekday)+2*j+parityOffset],
			})
		}
	}

	return week, nil
}
