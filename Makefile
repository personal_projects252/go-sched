##
# go-sched
#
# @file
# @version 0.1

all: build
	./sched

build:
	go build main.go
	mv ./main ./sched

# end
